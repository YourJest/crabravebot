const auth = require('./auth.js');
var db = require('./db_actions.js');

module.exports = {
	raveStart : raveStart,
	add_raver : add_raver,
	raveResults : raveResults,
	clearRavers : clearRavers
}

var rave_cooldown = null;

var ravers = [];
const dance_moves = ['left', 'right', 'up', 'down'];
var dance = [];

function clearRavers(){
	ravers = [];
	dance = [];
}
function add_raver(u_id, d_name, mov){
	if(raver_exist(u_id) === false || ravers.length === 0){
		let moves = [];
		for (var i = 0; i < mov.length; i++) {
			if(mov[i].match(/л/gi) !== null){
				moves.push("left");
			}else if (mov[i].match(/п/gi) !== null){
				moves.push("right");
			}else if (mov[i].match(/в/gi) !== null){
				moves.push("up");
			}else if (mov[i].match(/н/gi) !== null){
				moves.push("down");
			}
		}
		let sc = 0;
		let score = 0;
		for(var i = 0; i < moves.length; i++){
			if(moves[i] === dance[i]){
				sc++;
			}
		}
		score = sc * 25;
		if(sc === 4){
			score = 1000
		}
		console.log(u_id + " " + score)

		ravers.push({user_id : u_id, display_name: d_name, moves : moves, stones: score});
		console.log(ravers);
	}
}

function raver_exist(user_id){
	let re = ravers.find((el) => {
		if (el.user_id === user_id){
			console.log(el.user_id + " " + user_id)
			return true;
		}
	});
	if(re === undefined){
		return false;
	} else{
		return true;
	}
}

function raveStart(){
	for(i = 0; i <= 3; i++){
		dance.push(dance_moves[Math.floor(Math.random()*dance_moves.length)])
	}
	console.log(dance);
}

function raveResults(){
	let winners = [];
	db.upsert(ravers);
	for(i in ravers){
		if(ravers[i].stones === 500){
			winners.push(ravers[i])
		}
	}
	return {ravers: ravers, winners : winners, dance: dance};
}
