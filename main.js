var auth = require("./auth.js");
var ttv_rewards = require("./rewards.js");
var tmi = require("tmi.js");
var rave = require("./rave.js");
var loot = require("./lootler.js");
var db = require("./db_actions.js");
const fs = require("fs");
const WebSocket = require("ws");
const fetch = require("node-fetch");
const { URLSearchParams } = require("url");

const voices = ["oksana", "jane", "omazh", "zahar", "ermil"];
const say_syn = [
  " говорит: ",
  " мямлит: ",
  " вещает: ",
  " сообщает: ",
  " базарит :",
  " бормочет :",
  " выдаёт: ",
  " глаголит: ",
  " молвит: ",
];
const moves = "лпвн";

let users_attempts = [];
let soundsRegexp = {};
var tmi_options = {
  options: {
    debug: true,
  },
  connection: {
    cluster: "aws",
    reconnect: true,
  },
  identity: {
    username: "crabravebot",
    password: auth.botAuth,
  },
  channels: ["crabraveboy"],
};

let backendWsUrl = "ws://localhost:3030";
let twitchClient = new tmi.client(tmi_options);
let backendReconnectTimeout;
let backendWs;

const backendWsConnect = () => {
  console.log("Trying to connect to backend WebSocket");

  backendWs = new WebSocket(backendWsUrl);
  backendWs.onopen = function () {
    if (backendReconnectTimeout) {
      clearTimeout(backendReconnectTimeout);
    }
    let init_pl = { command_type: "init", whoami: "bot" };
    backendWs.send(JSON.stringify(init_pl));

    let soundReg_pl = { command_type: "get_sounds", whoami: "bot" };
    backendWs.send(JSON.stringify(soundReg_pl));
  };

  backendWs.onerror = () => {
    if (backendReconnectTimeout) {
      clearTimeout(backendReconnectTimeout);
    }
    backendReconnectTimeout = setTimeout(() => backendWsConnect(), 3000);
  };

  backendWs.onclose = () => {
    if (backendReconnectTimeout) {
      clearTimeout(backendReconnectTimeout);
    }
    backendReconnectTimeout = setTimeout(() => backendWsConnect(), 3000);
  };

  backendWs.on("message", (unparsed_data) => {
    let data = JSON.parse(unparsed_data);
    if (data.whoami === "server" && data.command_type === "switch_stones") {
      countStones = !countStones;
      console.log("countStones now: " + countStones);
    } else if (
      data.whoami === "server" &&
      data.command_type === "update_loolter"
    ) {
      loot.getInventory();
    } else if (
      data.whoami === "server" &&
      data.command_type === "reset_attempts"
    ) {
      users_attempts = [];
    } else if (data.whoami === "server" && data.command_type === "get_sounds") {
      soundsRegexp = data.sound_regexp;
      console.log(soundsRegexp);
    }
  });
};

backendWsConnect();

twitchClient.connect();
twitchClient.on("message", onMessageHandler);

function onMessageHandler(target, context, msg, self) {
  if (self) {
    return;
  }
  const commandName = msg.trim();
  if (commandName.match(/^!stones\b/gim) !== null) {
    db.getInfo([{ user_id: context["user-id"] }]).then((res) => {
      twitchClient.say(
        target,
        context["display-name"] + ", у тебя " + res[0].stones + " CUMУШКОВ"
      );
    });
  }
  if (context["msg-id"] !== "highlighted-message") {
    let soundsArr = [];
    for (sName in soundsRegexp) {
      let reg = commandName.match(
        new RegExp(soundsRegexp[sName].content, soundsRegexp[sName].flags)
      );
      if (reg) {
        soundsArr[reg.index] = sName;
      }
    }
    let filteredSounds = soundsArr.filter(Boolean);
    console.log(filteredSounds);
    for (i in filteredSounds) {
      playSound(filteredSounds[i]);
    }
  }
  if (context["custom-reward-id"] !== undefined) {
    switch (context["custom-reward-id"]) {
      case ttv_rewards.rave: {
        if (context["user-id"] == "96183015" && msg === "ыыыы") {
          let nikita_payload = {
            command_type: "tts_message",
            text: "Хаха привет всем я Никита и ыыыы ыыыы ыыыы сухрики пиво Уфа",
            whoami: "bot",
          };
          backendWs.send(JSON.stringify(nikita_payload));
          twitchClient.say(
            target,
            "НИКИТА КАК ЖЕ ТЫ РЕЙВИШЬ ВОТ ВСЕ ТАНЦУЮТ НОРМАЛЬНО А ТЫ ОПЯТЬ НАЫЫШИЛСЯ И СПИШЬ"
          );
          break;
        }
        let user_moves = msg.match(/[лпвн]{4}/gim);
        if (
          users_attempts[context["user-id"]] === undefined ||
          users_attempts[context["user-id"]].rave === undefined
        ) {
          users_attempts[context["user-id"]] = {};
          users_attempts[context["user-id"]].rave = 0;
        }
        if (users_attempts[context["user-id"]].rave < 5) {
          if (user_moves !== null) {
            //console.log(user_moves);
            let rave_moves = "";
            users_attempts[context["user-id"]].rave++;
            let rave_attempts_left =
              5 - users_attempts[context["user-id"]].rave;
            for (let i = 0; i < 4; i++) {
              rave_moves += moves[Math.floor(Math.random() * 4)];
            }
            if (user_moves[0].localeCompare(rave_moves) === 0) {
              twitchClient.say(
                target,
                "ОУ МАЙ, @" +
                  context["display-name"] +
                  ", ТЫ ПРОСТО ЮВЕЛИР ОТ МИРА ТАНЦЕВ!!!" +
                  "ТЫ ИДЕАЛЬНО ПОПАЛ В РЕЙВ И ТЕБЕ ПОЛОЖЕНА САБКА!!! ПИНАЙ ЭТОГО КРАБРЕЙВБОЯ, РАЗВОДИ ЕГО НА ДЕНЬГИ!!!" +
                  "Можешь потанцевать для удовольствия вот столько раз: " +
                  rave_attempts_left
              );
            } else {
              twitchClient.say(
                target,
                "@" +
                  context["display-name"] +
                  " НЕПЛОХИЕ ДВИЖЕНИЯ, МНЕ НРАВИТСЯ ОЧЕНЬ!!! Но в рэйв ты не попал:( Надо было так: " +
                  rave_moves +
                  ". Попыток рейвануть осталось: " +
                  rave_attempts_left
              );
            }
          } else {
            twitchClient.say(
              target,
              "Ты шо, @" +
                context["display-name"] +
                " НУ НАПИСАНО ЖЕ ввести движения рэйва! Теперь проси стримера вернуть камушки SMOrc"
            );
            break;
          }
        } else {
          twitchClient.say(
            target,
            "@" +
              context["display-name"] +
              ", ты израсходовал все попытки рейва на сегодня!!! Отдыхай ResidentSleeper"
          );
        }
        break;
      }
      case ttv_rewards.lootler: {
        if (
          users_attempts[context["user-id"]] === undefined ||
          users_attempts[context["user-id"]].lootler === undefined
        ) {
          users_attempts[context["user-id"]] = {};
          users_attempts[context["user-id"]].lootler = 0;
        }
        if (users_attempts[context["user-id"]].lootler < 5) {
          let regexp_steam =
            /(https:\/\/)?steamcommunity\.com\/tradeoffer\/new/gim;
          if (regexp_steam.test(msg)) {
            if (loot.isEmpty()) {
              twitchClient.say(
                target,
                context["display-name"] +
                  ", что-то пошло не так при вызове лутлера! Пни стримлера SMOrc."
              );
              return;
            }
            loot
              .lootler(context["user-id"], context["tmi-sent-ts"])
              .then((res) => {
                users_attempts[context["user-id"]].lootler++;
                let attempts_left =
                  5 - users_attempts[context["user-id"]].lootler;
                if (res.type === "weapon") {
                  if (res.weapon === null) {
                    twitchClient.say(
                      target,
                      context["display-name"] +
                        ", повезёт в следующий раз! Попыток залезть в сундук осталось : " +
                        attempts_left
                    );
                  } else {
                    twitchClient.say(
                      target,
                      context["display-name"] +
                        ", тебе очень повезло LUL Тебе выпала " +
                        res.weapon.weapon_name +
                        " за " +
                        res.weapon.weapon_price +
                        ". Попыток залезть в сундук осталось : " +
                        attempts_left
                    );
                  }
                }
              });
          } else {
            twitchClient.say(
              target,
              "Ты шо, @" +
                context["display-name"] +
                " НУ НАПИСАНО ЖЕ ввести ссылку на трейд стима! Теперь проси стримера вернуть камушки SMOrc"
            );
          }
        } else {
          twitchClient.say(
            target,
            "@" +
              context["display-name"] +
              ", налутался уже!!! РУКИ ПРОЧЬ ОТ СУНДУКА!!!"
          );
        }
      }
      default:
    }
  }
  if (context["msg-id"] === "highlighted-message") {
    let text =
      context["display-name"] +
      say_syn[Math.floor(Math.random() * say_syn.length)] +
      msg;
    let tts_payload = {
      command_type: "tts_message",
      text: text,
      whoami: "bot",
    };
    backendWs.send(JSON.stringify(tts_payload));
  }
}
function playSound(msg) {
  let sound_paylaod = {
    command_type: "chat_sound",
    sound_type: msg,
    whoami: "bot",
  };
  backendWs.send(JSON.stringify(sound_paylaod));
}
