module.exports = {
  lootler : lootler,
  getInventory : getInventory,
  isEmpty : isEmpty
}

var auth = require('./auth.js');
const fetch = require("node-fetch");


var users_cd = [];

var low_weapons = [];
var medium_weapons = [];
var high_weapons = [];
var legendary_weapons = [];

async function lootler(user_id){
  		let res = gimmeTheLoot();
    	return {type: 'weapon', weapon: res};
}

function isEmpty(){
  if(low_weapons.length === 0 || medium_weapons.length === 0 || high_weapons.length === 0 || legendary_weapons.length === 0 || isFetching){
    return true;
  }
  return false;
}

function gimmeTheLoot(){
  let rand = Math.random();
  console.log(rand);
  if (rand >= 0.97){
	let le_i = Math.floor(Math.random()*legendary_weapons.length);
	let le_w = legendary_weapons[le_i];
    legendary_weapons.splice(le_i, 1);
    return le_w;
    }
 	else if (rand >= 0.92){
	  let h_i = Math.floor(Math.random()*high_weapons.length);
	  let h_w = high_weapons[h_i];
    high_weapons.splice(h_i, 1);
    return h_w;
  }
  else if (rand >= 0.82){
	let m_i = Math.floor(Math.random()*medium_weapons.length);
  	let m_w = medium_weapons[m_i];
    medium_weapons.splice(m_i, 1);
    return m_w;
  }
  else if (rand >= 0.67){
	let lo_i = Math.floor(Math.random()*low_weapons.length);
	let lo_w = low_weapons[lo_i];
    low_weapons.splice(lo_i, 1);
   	return lo_w;
  }
  else if (rand < 0.67) {
  	return null;
  }
}

function getInventory(){
	var weapons = [];
	fetch('https://steamcommunity.com/profiles/76561198050394821/inventory/json/730/2',
        { method: 'GET'})
		.then(resp => resp.json())
    .then((response) => {
      isFetching = true;
		  var items = response.rgDescriptions;
      for(var key in items){
				if(items[key].tags.length >= 5){
					if(items[key].tags[4].internal_name.match(/Rarity_.*_Weapon/)){
						if (items[key].tradable === 1){
							weapons.push(items[key]);
						}
				}
			 }
  	  }
      let weapon_prices = [];
      low_weapons = [];
      medium_weapons = [];
      high_weapons = [];
      legendary_weapons = [];
      for(let i = 0; i < weapons.length; i++) {
        fetchPrice(weapons, i);
      }
    }).catch((error) => console.error(error));
}
function fetchPrice(weapons, i){
  setTimeout(() => {
    let market_name = weapons[i].market_hash_name;
    fetch('https://steamcommunity.com/market/priceoverview/?currency=5&appid=730&market_hash_name=' + encodeURIComponent(market_name),
        {method: 'GET'})
    .then(market_res =>  market_res.json())
    .then((market_response) => {
      let weapon_element = {
        weapon_id: weapons[i].classid + '_' + weapons[i].instanceid,
        weapon_name: market_name,
        weapon_price: market_response.lowest_price
      }
      let p_price = parseFloat(weapon_element.weapon_price.replace(/,/, "."));
      if (p_price <= 5){
        console.log("low, " + weapon_element.weapon_name);
        low_weapons.push(weapon_element);
      }else if(p_price <= 15){
        console.log("medium, " + weapon_element.weapon_name);
        medium_weapons.push(weapon_element);
      }else if(p_price <= 30){
        console.log("high, " + weapon_element.weapon_name)
        high_weapons.push(weapon_element);
      }else {
        console.log("legendary, " + weapon_element.weapon_name)
        legendary_weapons.push(weapon_element);
      }
      if(i === weapons.length - 1){
        isFetching = false;
      }
    }).catch(error => console.error(error));
  }, 4000 * i);

}
